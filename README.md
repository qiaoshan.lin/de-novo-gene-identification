### Trial 1 Find species specific genes expressed in flowers by [Bowtie2](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml) and [OrthoFinder](https://github.com/davidemms/OrthoFinder)
* [x] 1. Align flower/leaf RNAseq reads to CDS by bowtie2

> Run [t1/ref_index.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t1/ref_index.sh) first. Then run [t1/ml_align.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t1/ml_align.sh) and [t1/mv_align.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t1/mv_align.sh).

* [x] 2. Filter out CDS that is only expressed in flowers (only compare genes with >20 raw reads count in at least one library)

> Run [t1/ml_filter.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t1/ml_filter.sh) and [t1/mv_filter.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t1/mv_filter.sh). Remember to run the commands in comments after the array jobs end. 
    
See results in [LF10.flowerSpecificGene.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t1/LF10.flowerSpecificGene.txt) (4221 transcripts) and [MvBL.flowerSpecificGene.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t1/MvBL.flowerSpecificGene.txt) (3375 transcripts).

* [x] 3. Run orthoFinder for *Mimulus lewisii* complex and *guttatus* with three other species *```failed: no orthogroup found```*

>  Download [tomato proteins](ftp://ftp.solgenomics.net/tomato_genome/annotation/ITAG3.2_release/ITAG3.2_proteins.fasta), [Arabidopsis proteins](https://www.arabidopsis.org/download_files/Proteins/Araport11_protein_lists/Araport11_genes.201606.pep.fasta.gz), [snapdragon proteins](http://bioinfo.sibs.ac.cn/Am/download_data_genome_v3/Amajus.IGDBv3.pros.fasta.gz).
>  Then run [t1/orthofinder.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t1/orthofinder.sh).

* [x] 4. Filter out CDS that has no functional annotation and is unassigned to any *Mimulus* orthogroup

> Run the ending block in [t1/ml_filter.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t1/ml_filter.sh).

See results in [LF10.flowerSpecificGene.noanno.unassigned.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t1/LF10.flowerSpecificGene.noanno.unassigned.txt) (98 transcripts).

### Trial 2 Figure out why no orthogroup was found in Trial 1 *```no longer a problem; results deleted```*
* [x] 1. Run orthoFinder for *Mimulus lewisii* complex, *guttatus*, and snapdragon

> Run [t2/orthofinder.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t2/orthofinder.sh).

OrthoFinder assigned 156208 genes (81.3% of total) to 23402 orthogroups. Fifty percent of all genes were in orthogroups with 6 or more genes (G50 was 6) and were contained in the largest 9282 orthogroups (O50 was 9282). There were 1539 orthogroups with all species present and 293 of these consisted entirely of single-copy genes.

* [x] 2. Run orthoFinder for *Mimulus lewisii* complex, *guttatus*, snapdragon, and tomato

> Run [t2/orthofinder2.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t2/orthofinder2.sh).

OrthoFinder assigned 184311 genes (80.8% of total) to 22572 orthogroups. Fifty percent of all genes were in orthogroups with 7 or more genes (G50 was 7) and were contained in the largest 8386 orthogroups (O50 was 8386). There were 1398 orthogroups with all species present and 138 of these consisted entirely of single-copy genes.

* [x] 3. Run orthoFinder for *Mimulus lewisii* complex, *guttatus*, snapdragon, tomato, and Arabidopsis

> Run [t2/orthofinder3.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t2/orthofinder3.sh)

OrthoFinder assigned 223915 genes (81.0% of total) to 21679 orthogroups. Fifty percent of all genes were in orthogroups with 10 or more genes (G50 was 10) and were contained in the largest 7310 orthogroups (O50 was 7310). There were 1194 orthogroups with all species present and 36 of these consisted entirely of single-copy genes.

##### :exclamation: The failure of trial 1 was due to the wrong protein input files. After reload the inputs and rerun orthoFinder in trial 1, it produced the same results as step 3 in trial 2.

### Trial 3 Find species specific genes expressed in flowers by Salmon

* [x] 1. Quantify the gene expression levels in different samples

> Run [t3/index.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/index.sh) and then [t3/salmon_ml.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/salmon_ml.sh) and [t3/salmon_mv.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/salmon_mv.sh).

| Sample | Bowtie2 alignment rate | Salmon alignment rate |
| ------ | ------ | ------ |
| trimmed_LF10_10FB_ATCACG_L003 | 74.74% | 72.61% |
| trimmed_LF10_15FB_CGATGT_L003 | 77.4806% | 75.26% |
| trimmed_LF10_15mm_Corolla | 76.8521% | 76.34% |
| trimmed_LF10_15NG_GTGAAA_L001 | 58.2775% | 56.47% |
| trimmed_LF10_15PL_GTCCGC_L001 | 76.6462% | 74.49% |
| trimmed_LF10_20FB_TTAGGC_L003 | 78.01% | 75.56% |
| trimmed_LF10_2FB_ACTGAT_L002 | 78.01% | 75.02% |
| trimmed_LF10L_CGATGT_L008 | 77.208% | 77.49% |
| trimmed_MvBL_FB10 | 78.2982% | 71.13% |
| trimmed_MvBL_FB16 | 76.1196% | 73.67% |
| trimmed_MvBL_FB30 | 78.4337% | 72.90% |
| trimmed_MvBL_FB5 | 76.5707% | 71.93% |
| trimmed_MvBL_TinyFB | 76.791% | 71.99% |
| trimmed_MvBL_YoungLeaf | 77.3538% | 72.75% |

* [x] 2. Filter the gene lists by the following criteria
* only expressed in flowers or expressed in flowers at least 10 times higher than in leaves
* discard genes expressed in leaves with TPM >= 10
* discard genes that have orthologues to the other species tested in trial2
* discard genes that have functional annotations

> Run [t3/filter.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/filter.sh).

| Result list | Gene count |
| ------ | ------ |
| [LF10.flowerSpecific.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/LF10.flowerSpecific.txt) | 4368 |
| [LF10.flowerSpecific.unassign.anno.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/LF10.flowerSpecific.unassign.anno.txt) | 17 |
| [LF10.flowerSpecific.unassign.noanno.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/LF10.flowerSpecific.unassign.noanno.txt) | 132 |
| [LF10.flowerSpecific.unassign.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/LF10.flowerSpecific.unassign.txt) | 149 |
| [LF10.flowerSpecific.unassign.intronFree.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/LF10.flowerSpecific.unassign.intronFree.txt) | 49 |
| [LF10.flowerSpecific.unassign.intronFree.repeats.intersect.bed](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/LF10.flowerSpecific.unassign.intronFree.repeats.intersect.bed) | 3 |

| Result list | Gene count |
| ------ | ------ |
| [MvBL.flowerSpecific.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/MvBL.flowerSpecific.txt) | 3375 |
| [MvBL.flowerSpecific.unassign.anno.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/MvBL.flowerSpecific.unassign.anno.txt) | 152 |
| [MvBL.flowerSpecific.unassign.noanno.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/MvBL.flowerSpecific.unassign.noanno.txt) | 205 |
| [MvBL.flowerSpecific.unassign.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/MvBL.flowerSpecific.unassign.txt) | 357 |
| [MvBL.flowerSpecific.unassign.intronFree.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/MvBL.flowerSpecific.unassign.intronFree.txt) | 84 |
| [MvBL.flowerSpecific.unassign.intronFree.repeats.intersect.bed](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t3/MvBL.flowerSpecific.unassign.intronFree.repeats.intersect.bed) | 11 |

### Trial 4 Find *Mimulus leiwsii* complex specific genes expressed in flowers

* [x] 1. Quantify the gene expression levels in different samples

> Use the results from trial 3. 

* [x] 2. Filter the gene lists by the following criteria
* discard genes that have orthologues to the other species (*M. guttatus*, snapdragon, tomato, *Arabidopsis*) tested in trial 2
* single copy orthogroups
* expressed in flowers

> Run [t4/filter.sh](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t4/filter.sh).

| Result list | Gene count |
| ------ | ------ |
| [mimulusSpecific.orthogroups.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t4/mimulusSpecific.orthogroups.txt) | 611 (groups) |
| [mimulusSpecific.orthogroups.singleCopy.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t4/mimulusSpecific.orthogroups.singleCopy.txt) | 178 (groups) |
| [LF10.flower.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t4/LF10.flower.txt) | 26513 |
| [MvBL.flower.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t4/MvBL.flower.txt) | 25342 |
| [LF10.mimulusSpecific.flower.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t4/LF10.mimulusSpecific.flower.txt) | 89 |
| [MvBL.mimulusSpecific.flower.txt](https://gitlab.com/qiaoshan.lin/de-novo-gene-identification/blob/master/t4/MvBL.mimulusSpecific.flower.txt) | 79 |

* [ ] 3. Create a table of gene expression levels across flower developmental stages and dN/dS ratios.


