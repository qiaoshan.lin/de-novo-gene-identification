# discard lewisii complex orthogroups that have orthologues to the other species
awk '$4*$6*$8*$9>=1&&$2+$3+$5+$7==0{print}' ../t1/genomes/Results_Aug08/Orthogroups.GeneCount.csv |cut -f1> mimulusSpecific.orthogroups.txt 
# discard orthogroups that are not single copy orthologues
awk '$4*$6*$8*$9==1&&$2+$3+$5+$7==0{print}' ../t1/genomes/Results_Aug08/Orthogroups.GeneCount.csv |cut -f1> mimulusSpecific.orthogroups.singleCopy.txt
# get genes for the orthogroups
group=()
while read line; do group+=("$line"); done < mimulusSpecific.orthogroups.singleCopy.txt
for g in ${group[@]}
do 
	grep $g ../t1/genomes/Results_Aug08/Orthogroups.csv | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"}' >> mimulusSpecific.orthologues.txt
done

# the last column of mimulusSpecific.orthologues.txt has a strange space, which will influence the next step. So we need to delete the space.
sed -i 's/\s*$//' mimulusSpecific.orthologues.txt

# seperate the orthologues by species
cut -f2 mimulusSpecific.orthologues.txt > CE10.mimulusSpecific.txt
cut -f3 mimulusSpecific.orthologues.txt > LF10.mimulusSpecific.txt
cut -f4 mimulusSpecific.orthologues.txt > Mpar.mimulusSpecific.txt
cut -f5 mimulusSpecific.orthologues.txt > MvBL.mimulusSpecific.txt

# find genes expressing in flowers
flower=(../t3/trimmed_LF10_*_quant/quant.sf)
leaf=(../t3/trimmed_LF10L*_quant/quant.sf)

for f in ${flower[@]}
do
	join -j 1 --nocheck-order --header ${flower[0]} $leaf |tr ' ' '\t'|tail -n +2|awk '$4>0{print $1}' >> LF10.flower.tmp
done

sort LF10.flower.tmp | uniq > LF10.flower.txt
rm LF10.flower.tmp

flower=(../t3/trimmed_MvBL_*FB*_quant/quant.sf)
leaf=(../t3/trimmed_MvBL_YoungLeaf*_quant/quant.sf)

for f in ${flower[@]}
do
	join -j 1 --nocheck-order --header ${flower[0]} $leaf |tr ' ' '\t'|tail -n +2|awk '$4>0{print $1}' >> MvBL.flower.tmp
done

sort MvBL.flower.tmp | uniq > MvBL.flower.txt
rm MvBL.flower.tmp

# find the intercept of genes expressing in flowers and are single copy orthologues in lewisii complex while having no orthologues to the other species
cat LF10.flower.txt LF10.mimulusSpecific.txt |sort|uniq -c|awk '$1==2{print $2}' > LF10.mimulusSpecific.flower.txt
cat MvBL.flower.txt MvBL.mimulusSpecific.txt |sort|uniq -c|awk '$1==2{print $2}' > MvBL.mimulusSpecific.flower.txt

# get genes having no annotations
cat ~/resource/LF10/LF10g_v2.0_functional_annotations.tsv |awk '$2==""{print}' > LF10.noanno.txt
cat ~/resource/CE10/CE10g_v2.0_functional_annotations.tsv |awk '$2==""{print}' > CE10.noanno.txt
cat ~/resource/parisii/Mparg_v2.0_functional_annotations.tsv |awk '$2==""{print}' > Mpar.noanno.txt
cat ~/resource/mvbl/MvBLg_v2.0_functional_annotations.tsv |awk '$2==""{print}' > MvBL.noanno.txt






