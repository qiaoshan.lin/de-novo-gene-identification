#!/bin/bash
#SBATCH --job-name=orthofinder2
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o orthofinder2_%j.out
#SBATCH -e orthofinder2_%j.err

module load OrthoFinder/2.2.7
module load diamond
module load mcl
module load FastME
module load blast

mkdir tomato
mv ../t1/ITAG3.2_proteins.fasta tomato/
orthofinder -t 8 -b genomes/Results_Aug03/WorkingDirectory -f tomato

