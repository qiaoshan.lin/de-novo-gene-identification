#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o orthofinder_%j.out
#SBATCH -e orthofinder_%j.err

module load OrthoFinder/2.2.7
module load diamond
module load mcl
module load FastME
module load blast

orthofinder -t 8 -a 4 -f genomes


