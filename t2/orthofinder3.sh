#!/bin/bash
#SBATCH --job-name=orthofinder2
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 12
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o orthofinder3_%j.out
#SBATCH -e orthofinder3_%j.err

module load OrthoFinder/2.2.7
module load diamond
module load mcl
module load FastME
module load blast

mkdir Arabidopsis
mv ../t1/Araport11_genes.201606.pep.fasta Arabidopsis/
orthofinder -t 12 -b genomes/Results_Aug03/WorkingDirectory -f Arabidopsis

