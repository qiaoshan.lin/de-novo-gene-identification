# filter by these criteria:
# 1: expressed in flowers at least 10 times higher than in leaves (or only expressed in flowers)
# 2: discard genes expressed in leaves with TPM >= 10
# 3: discard genes that have orthologues to the other species
# 4: discard genes that have functional annotations

# get the average of TPM in all samples
cat trimmed_LF10*_quant/quant.sf|cut -f4|grep -v 'TPM'|perl -lane '{$sum+=$_}END{print $sum/$.}'
cat trimmed_MvBL*_quant/quant.sf|cut -f4|grep -v 'TPM'|perl -lane '{$sum+=$_}END{print $sum/$.}'

flower=(trimmed_LF10_*_quant/quant.sf)
leaf=(trimmed_LF10L*_quant/quant.sf)

for f in ${flower[@]}
do
# filter 1
	join -j 1 --nocheck-order --header ${flower[0]} $leaf |tr ' ' '\t'|tail -n +2|awk '{if($4>0&&$8==0)print $1"\t"$8;else if($8>0&&$4/$8>=10)print $1"\t"$8}' > tmp
# filter 2
	awk '$2<10{print $1}' tmp >> LF10.flowerSpecific.tmp
done

sort LF10.flowerSpecific.tmp | uniq > LF10.flowerSpecific.txt
rm tmp LF10.flowerSpecific.tmp

flower=(trimmed_MvBL_*FB*_quant/quant.sf)
leaf=(trimmed_MvBL_YoungLeaf*_quant/quant.sf)

for f in ${flower[@]}
do
# filter 1
        join -j 1 --nocheck-order --header ${flower[0]} $leaf |tr ' ' '\t'|tail -n +2|awk '{if($4>0&&$8==0)print $1"\t"$8;else if($8>0&&$4/$8>=10)print $1"\t"$8}' > tmp
# filter 2
        awk '$2<10{print $1}' tmp >> MvBL.flowerSpecific.tmp
done

sort MvBL.flowerSpecific.tmp | uniq > MvBL.flowerSpecific.txt
rm tmp MvBL.flowerSpecific.tmp

orthoUnassign=../t2/genomes/Results_Aug03/WorkingDirectory/Orthogroups_2_UnassignedGenes.csv 

# filter 3
gene=()
while read line; do gene+=("$line"); done < LF10.flowerSpecific.txt
for g in ${gene[@]}
do
	if [[ `grep $g $orthoUnassign` ]]; then echo $g >> LF10.flowerSpecific.unassign.txt; fi
done

gene=()
while read line; do gene+=("$line"); done < MvBL.flowerSpecific.txt
for g in ${gene[@]}
do
        if [[ `grep $g $orthoUnassign` ]]; then echo $g >> MvBL.flowerSpecific.unassign.txt; fi
done

# filter 4 

gene=()
while read line; do gene+=("$line"); done < LF10.flowerSpecific.unassign.txt
for g in ${gene[@]}; do grep $g ~/resource/LF10/LF10g_v2.0_functional_annotations.tsv |awk '$2==""{print}'; done |sort > LF10.flowerSpecific.unassign.noanno.txt

gene=()
while read line; do gene+=("$line"); done < MvBL.flowerSpecific.unassign.txt
for g in ${gene[@]}; do grep $g ~/resource/mvbl/MvBLg_v2.0_functional_annotations.tsv |awk '$2==""{print}'; done |sort > MvBL.flowerSpecific.unassign.noanno.txt

# what are the annotations for the orthogroup unassigned genes?
gene=()
while read line; do gene+=("$line"); done < LF10.flowerSpecific.unassign.txt
for g in ${gene[@]}; do grep $g ~/resource/LF10/LF10g_v2.0_functional_annotations.tsv |awk '$2!=""{print}'; done > LF10.flowerSpecific.unassign.anno.txt

gene=()
while read line; do gene+=("$line"); done < MvBL.flowerSpecific.unassign.txt
for g in ${gene[@]}; do grep $g ~/resource/mvbl/MvBLg_v2.0_functional_annotations.tsv |awk '$2!=""{print}'; done > MvBL.flowerSpecific.unassign.anno.txt

# find intron free genes
awk '$3=="intron"{print $10}' ~/resource/LF10/LF10g_v2.0.gtf |sort|uniq|tr -d '"'|tr -d ';' > LF10.intron.txt
cat LF10.intron.txt LF10.flowerSpecific.unassign.txt |sort|uniq -c|awk '$1==2{print $2}'|cat - LF10.flowerSpecific.unassign.txt |sort|uniq -c|awk '$1==1{print $2}' > LF10.flowerSpecific.unassign.intronFree.txt

awk '$3=="intron"{print $10}' ~/resource/mvbl/MvBLg_v2.0.gtf |sort|uniq|tr -d '"'|tr -d ';' > MvBL.intron.txt
cat MvBL.intron.txt MvBL.flowerSpecific.unassign.txt |sort|uniq -c|awk '$1==2{print $2}'|cat - MvBL.flowerSpecific.unassign.txt |sort|uniq -c|awk '$1==1{print $2}' > MvBL.flowerSpecific.unassign.intronFree.txt

# find genes overlaped with repeats
tail -n +4 /home/CAM/qlin/LF10_Genome/versions/chromosomes/repeatMasker_20190215_hard/LF10g_chr.fa.out |awk '{print $5"\t"$6"\t"$7"\t"$10}' > LF10.repeats.bed
awk '$3=="transcript"{print $1,$4,$5,$10}' OFS='\t' ~/resource/LF10/LF10g_v2.0.gtf |sed 's/"//g'|sed 's/^LF10_//'|sed 's/;$//' > LF10.genes.bed
gene=()
while read line; do gene+=("$line"); done < LF10.flowerSpecific.unassign.intronFree.txt
for g in ${gene[@]}; do grep $g LF10.genes.bed; done > LF10.flowerSpecific.unassign.intronFree.bed
bedtools intersect -a LF10.flowerSpecific.unassign.intronFree.bed -b LF10.repeats.bed -wo > LF10.flowerSpecific.unassign.intronFree.repeats.intersect.bed

tail -n +4 /home/CAM/qlin/MVBL_Genome/versions/chromosomes/repeatMasker_20190215_hard/MvBLg_chr.fa.out |awk '{print $5"\t"$6"\t"$7"\t"$10}' > MvBL.repeats.bed 
awk '$3=="transcript"{print $1,$4,$5,$10}' OFS='\t' ~/resource/mvbl/MvBLg_v2.0.gtf |sed 's/"//g'|sed 's/^MvBL_//'|sed 's/;$//' > MvBL.genes.bed
gene=()
while read line; do gene+=("$line"); done < MvBL.flowerSpecific.unassign.intronFree.txt
for g in ${gene[@]}; do grep $g MvBL.genes.bed; done > MvBL.flowerSpecific.unassign.intronFree.bed
bedtools intersect -a MvBL.flowerSpecific.unassign.intronFree.bed -b MvBL.repeats.bed -wo > MvBL.flowerSpecific.unassign.intronFree.repeats.intersect.bed



