#!/bin/bash
#SBATCH --job-name=salmon_mv
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-5
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o salmon_mv_%A_%a.out
#SBATCH -e salmon_mv_%A_%a.err

export PATH=$PATH:~/local/apps/salmon-latest_linux_x86_64/bin/

file=(trimmed_MvBL_FB10 trimmed_MvBL_FB16 trimmed_MvBL_FB30 trimmed_MvBL_FB5 trimmed_MvBL_TinyFB trimmed_MvBL_YoungLeaf)

dir=/home/CAM/qlin/MVBL_Genome/10_RNAseq_reads_trimming/t1

salmon quant -i MvBL_index -l A \
         -1 $dir/${file[$SLURM_ARRAY_TASK_ID]}_R1_paired.fq.gz \
         -2 $dir/${file[$SLURM_ARRAY_TASK_ID]}_R2_paired.fq.gz \
         -p 8 --gcBias --validateMappings --seqBias -o ${file[$SLURM_ARRAY_TASK_ID]}_quant


