#!/bin/bash
#SBATCH --job-name=index
#SBATCH -n 8
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o index_%j.out
#SBATCH -e index_%j.err

export PATH=$PATH:~/local/apps/salmon-latest_linux_x86_64/bin/

transcriptome=/home/CAM/qlin/resource/LF10/LF10g_v2.0.codingseq.fa
salmon index -t $transcriptome -i LF10_index -p 8

transcriptome=/home/CAM/qlin/resource/mvbl/MvBLg_v2.0.codingseq.fa
salmon index -t $transcriptome -i MvBL_index -p 8

