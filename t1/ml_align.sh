#!/bin/bash
#SBATCH --job-name=align
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-13
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o align_%A_%a.out
#SBATCH -e align_%A_%a.err

module load bowtie2
module load samtools

LF10=/home/CAM/qlin/resource/LF10/LF10g_v2.0.codingseq.fa

file=(trimmed_LF10_10FB_ATCACG_L003 trimmed_LF10_15FB_CGATGT_L003 trimmed_LF10_15mm_Corolla trimmed_LF10_15NG_GTGAAA_L001 trimmed_LF10_15PL_GTCCGC_L001 trimmed_LF10_20FB_TTAGGC_L003 trimmed_LF10_2FB_ACTGAT_L002 trimmed_LF10L_CGATGT_L008 trimmed_LF10_FB13A trimmed_LF10_FB13B trimmed_LF10_FB13C trimmed_LF10_FB5A trimmed_LF10_FB5B trimmed_LF10_FB5C)

LF10dir1=/home/CAM/qlin/LF10_Genome/10_RNAseq_reads_trimming/t1
LF10dir2=/home/CAM/qlin/RCP1_Overexpression_RNAseq_Analysis/02_trim/t3/

dir=($LF10dir1 $LF10dir1 $LF10dir1 $LF10dir1 $LF10dir1 $LF10dir1 $LF10dir1 $LF10dir1 $LF10dir2 $LF10dir2 $LF10dir2 $LF10dir2 $LF10dir2 $LF10dir2)

bowtie2 --threads 8 --phred33 --no-discordant --no-unal -x LF10 -1 ${dir[$SLURM_ARRAY_TASK_ID]}/${file[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz -2 ${dir[$SLURM_ARRAY_TASK_ID]}/${file[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz -S ${file[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bST $LF10 -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.sam > ${file[$SLURM_ARRAY_TASK_ID]}.bam
samtools sort -@ 8 -o ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam -T ${file[$SLURM_ARRAY_TASK_ID]}tmp -O bam -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.bam
samtools index ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam

rm ${file[$SLURM_ARRAY_TASK_ID]}.bam ${file[$SLURM_ARRAY_TASK_ID]}.sam


