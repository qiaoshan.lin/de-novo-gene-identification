#!/bin/bash
#SBATCH --job-name=ref_index
#SBATCH -c 4
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o ref_index_%j.out
#SBATCH -e ref_index_%j.err

module load bowtie2
module load samtools

LF10=/home/CAM/qlin/resource/LF10/LF10g_v2.0.codingseq.fa

bowtie2-build --quiet --threads 8 $LF10 LF10

MvBL=/home/CAM/qlin/resource/mvbl/MvBLg_v2.0.codingseq.fa

bowtie2-build --quiet --threads 8 $MvBL MvBL

