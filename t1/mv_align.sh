#!/bin/bash
#SBATCH --job-name=align
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-5
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o align_%A_%a.out
#SBATCH -e align_%A_%a.err

module load bowtie2
module load samtools

MvBL=/home/CAM/qlin/resource/mvbl/MvBLg_v2.0.codingseq.fa

file=(trimmed_MvBL_FB10 trimmed_MvBL_FB16 trimmed_MvBL_FB30 trimmed_MvBL_FB5 trimmed_MvBL_TinyFB trimmed_MvBL_YoungLeaf)

MvBLdir=/home/CAM/qlin/MVBL_Genome/10_RNAseq_reads_trimming/t1

bowtie2 --threads 8 --phred33 --no-discordant --no-unal -x MvBL -1 $MvBLdir/${file[$SLURM_ARRAY_TASK_ID]}_R1_paired.fq.gz -2 $MvBLdir/${file[$SLURM_ARRAY_TASK_ID]}_R2_paired.fq.gz -S ${file[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bST $MvBL -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.sam > ${file[$SLURM_ARRAY_TASK_ID]}.bam
samtools sort -@ 8 -o ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam -T ${file[$SLURM_ARRAY_TASK_ID]}tmp -O bam ${file[$SLURM_ARRAY_TASK_ID]}.bam
samtools index ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam

rm ${file[$SLURM_ARRAY_TASK_ID]}.bam ${file[$SLURM_ARRAY_TASK_ID]}.sam


