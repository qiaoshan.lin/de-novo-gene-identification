#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -c 2
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-5
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o mv_filter_%A_%a.out
#SBATCH -e mv_filter_%A_%a.err

module load bowtie2
module load samtools

MvBL=/home/CAM/qlin/resource/mvbl/MvBLg_v2.0.codingseq.fa

file=(trimmed_MvBL_FB10 trimmed_MvBL_FB16 trimmed_MvBL_FB30 trimmed_MvBL_FB5 trimmed_MvBL_TinyFB trimmed_MvBL_YoungLeaf)

samtools view ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam|awk '$5>20{print}'|cut -f3|uniq -c|awk '{print $2"\t"$1}' > ${file[$SLURM_ARRAY_TASK_ID]}.count

# run the following commands after the array jobs end
# file=(*MvBL*FB*count)
# for f in ${file[@]}; do awk '$2>20{print}' $f >> MvBL_flower.count; done 
# awk '$2>20{print}' *MvBL_YoungLeaf*count > MvBL_leaf.count
# cut -f1 MvBL_flower.count|sort|uniq|cat - MvBL_leaf.count|cut -f1|sort|uniq -c|awk '$1==2{print $2}' > intersection
# cut -f1 MvBL_flower.count|sort|uniq|cat - intersection|sort|uniq -c|awk '$1==1{print $2}' > MvBL.flowerSpecificGene.txt
# rm intersection
# perl ~/scripts/fetchSeq4fa2.pl ~/resource/mvbl/MvBLg_v2.0.protein.fa MvBL.flowerSpecificGene.txt MvBL.flowerSpecificGene.fasta


