#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -c 2
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-13
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o ml_filter_%A_%a.out
#SBATCH -e ml_filter_%A_%a.err

module load bowtie2
module load samtools

LF10=/home/CAM/qlin/resource/LF10/LF10g_v2.0.codingseq.fa

file=(trimmed_LF10_10FB_ATCACG_L003 trimmed_LF10_15FB_CGATGT_L003 trimmed_LF10_15mm_Corolla trimmed_LF10_15NG_GTGAAA_L001 trimmed_LF10_15PL_GTCCGC_L001 trimmed_LF10_20FB_TTAGGC_L003 trimmed_LF10_2FB_ACTGAT_L002 trimmed_LF10L_CGATGT_L008 trimmed_LF10_FB13A trimmed_LF10_FB13B trimmed_LF10_FB13C trimmed_LF10_FB5A trimmed_LF10_FB5B trimmed_LF10_FB5C)

samtools view ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam|awk '$5>20{print}'|cut -f3|uniq -c|awk '{print $2"\t"$1}' > ${file[$SLURM_ARRAY_TASK_ID]}.count

# run the following commands after the array jobs end
# file=(*LF10_*count)
# for f in ${file[@]}; do awk '$2>20{print}' $f >> LF10_flower.count; done 
# awk '$2>20{print}' *LF10L*count > LF10_leaf.count
# cut -f1 LF10_flower.count|sort|uniq|cat - LF10_leaf.count|cut -f1|sort|uniq -c|awk '$1==2{print $2}' > intersection
# cut -f1 LF10_flower.count|sort|uniq|cat - intersection|sort|uniq -c|awk '$1==1{print $2}' > LF10.flowerSpecificGene.txt
# rm intersection
# perl ~/scripts/fetchSeq4fa2.pl ~/resource/LF10/LF10g_v2.0.protein.fa LF10.flowerSpecificGene.txt LF10.flowerSpecificGene.fasta
# cut -f1 LF10.flowerSpecificGene.txt -d '.'|sort|join -j 1 - ~/resource/LF10/LF10g_v2.0_known_gene_anno|grep '_'|cut -f2- -d '_' > LF10.flowerSpecificGene.known.txt

########### following is Step 4 #############
# array=()
# while read line; do array+=("$line") ; done < LF10.flowerSpecificGene.txt
# for a in ${array[@]}; do grep $a ~/resource/LF10/LF10g_v2.0_functional_annotations.tsv |awk '$2==""{print}'; done |sort > LF10.flowerSpecificGene.noanno.txt
# cut -f3 ~/LF10_Genome/15_orthoFinder/t6/genomes/Results_Jun29/Orthogroups_UnassignedGenes.csv |grep '^ML'|sort|join -j 1 - LF10.flowerSpecificGene.noanno.txt > LF10.flowerSpecificGene.noanno.t6unassigned.txt
# cut -f3 ~/LF10_Genome/15_orthoFinder/t4/genomes/Results_Jun18/Orthogroups_UnassignedGenes.csv |grep '^ML'|sort|join -j 1 - LF10.flowerSpecificGene.noanno.t6unassigned.txt> LF10.flowerSpecificGene.noanno.unassigned.txt
# rm LF10.flowerSpecificGene.noanno.t6unassigned.txt

